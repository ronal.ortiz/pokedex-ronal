# Ejercicio práctico front-end Pokédex - Ronal Ortiz

Implementación de una interfaz web responsive base de un Pokédex.
Realizado en HTML y CSS.

## Método aplicados

Se hizo uso de los siguiente métodos:
* HTML5 semántico
* CSS responsive (Con la filosofía de desarrollo Mobile first y BEM).

## Responsable

Ronal Ortiz Rivero